def say_hello(name)
  puts "Hello #{name}"
end

say_hello "Peter"
say_hello "Paul"
say_hello "Mary"
