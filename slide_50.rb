def chapter(title, number: 1, body: nil)
  puts "Chapter #{number}: #{title}"
  puts body if body
end

puts chapter "The beginning"
puts chapter "The next chapter", number: 2
puts chapter "A long one", number: 3, body: "A long long time ago..."
