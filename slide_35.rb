puts "hello world".sub('hello','goodbye')

# Replace all occurrences of a String
p "one two one".sub('one','three')

p "one two one".gsub('one','three')

# Get a "slice" of a String
puts "hello world".slice(0)

puts "hello world".slice(-1)

puts "hello world".slice(4,4)
