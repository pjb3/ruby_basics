x = 42
p x

if x
  puts "yes"
else
  puts "no"
end

x = 0
p x

if x
  puts "yes"
else
  puts "no"
end

x = "false"
p x

if x
  p 1
else
  p 0
end

x = false
p x

if x
  puts 1
else
  puts 0
end

x = nil
p x

if x
  puts 1
else
  puts 0
end
