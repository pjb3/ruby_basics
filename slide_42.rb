person = { name: "Paul", age: 35 }

p person.merge(name: "Paul Barry", city: "Baltimore")

# merge returns a new Hash, does not modify the Hash
p person

#merge! modifies the Hash
p person.merge!(name: "Paul Barry", city: "Baltimore")

p person
