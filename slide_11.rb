# Variables are named referenced to objects. You create a variable by assigning an Object to it.

my_name = "Paul"

puts my_name

puts my_name == "Paul"

puts my_name == 42

# Shorthand for incrementing a variable
i = 0
i += 1
puts i

#Equivalent to
i = i + 1
puts i
