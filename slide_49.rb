def multiply(*args)
  args.reduce(:*)
end

puts multiply(2)
puts multiply(2, 3)
puts multiply(2, 3, 4)
puts multiply(2, 3, 4, 5)
