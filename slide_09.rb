# The + operator concatenates strings together into a new string
puts "Hello" + " " + "World"

# The << operator appends to a string
puts "Hello" << " World"

# Interpolation allows you to include the result of evaluating an expression in a String
puts "40 + 2 = #{40 + 2}"

# If you don't want interpolation to occur, use single quotes or precede the # with \
puts '40 + 2 = #{40 + 2}'
