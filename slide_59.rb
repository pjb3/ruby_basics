def say_hello(name)
  puts "Hello, #{name}!"
end

people = ["Peter", "Paul", "Mary"]
people << "Bob"

for person in people
  say_hello person
end
