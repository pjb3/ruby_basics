def circumference(radius)
  2 * 3.141592653589793 * radius
end

PI = 3.141592653589793

def circumference(radius)
  2 * PI * radius
end

puts circumference(7)
