def say_hello(name)
  puts "Hello #{name}"
end

people = ["Peter", "Paul", "Mary"]

for person in people
  say_hello person
end
