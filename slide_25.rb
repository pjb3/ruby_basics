p /oo/.class

p "food" =~ /oo/

p "food" =~ /^foo/

p "food" =~ /d$/

p "food" =~ /foo$/

p "food" !~ /foo$/
