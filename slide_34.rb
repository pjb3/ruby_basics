puts "hello".capitalize

puts "hello".upcase

puts "hello".upcase.downcase

# Split a String into an Array
p "hello word".split

p "hello, world!".split(',')

# Remove whitespace from the beginning and the end
puts "  hello  ".strip
