a = [42, 99, 100]
p a.delete 100

p a

# Find the index of an element in the Array
p a.index(99)

# Remove an element from the Array by index
p a.delete_at(1)

p a
