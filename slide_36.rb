# Convert to an Integer
puts "42".to_i

# Convert to a Float
puts "3.14159".to_f

# Convert to a Symbol
puts "hello".to_sym
