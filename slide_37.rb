p 1.even?

p 1.odd?

# Get the absolute value of a number
p -42.abs

# Round a decimal
p 3.14159.round
p 3.14159.round(2)
