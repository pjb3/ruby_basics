def say_hello(name)
  puts "Hello #{name}"
end

say_hello "Paul"
begin
  puts name
rescue NameError => ex
  puts "oops, there was a #{ex.class} exception"
end
say_hello "Peter"
