# Create a copy of an Object
my_name = "John"
p my_name

name = my_name.dup
p name

my_name << " Doe"

# The original Object has been modified
p my_name

# The copy has not
p name
