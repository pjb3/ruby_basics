person = { name: "Paul" }
p person
p person[:name]
p person["name"]

person = { "name" => "Paul" }
p person
p person["name"]
p person[:name]
